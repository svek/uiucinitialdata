// Choose the initial lapse from parameter initial_lapse_type
static int lapse_type = 0 ;
// 0 : stationary
// 1 : unit
// 2 : trumpet
// 3 : iso_schwarzschild
// 4 : puncture

// Choose the initial shift from parameter initial_shift_type
static int shift_type = 1;
// 0 : zero
// 1 : stationary
// 2 : radial Gaussian

static int BH_gauge_choice = 0;
// 0 : UIUC Liu - Shapiro - Etienne 2009
// 1 : Quasi-isotropic


//For a single BH = location of the puncture
static double center_offset[3] {
  0., // x offset
  0., // y offset
  0.  // z offset
};

//limit r to r_min to not evaluate on the puncture
static bool avoid_puncture = true;

// BH Mass in M_sun
static double par_M = 2.0;

//dimensionless spin J/M^2
static double par_chi = 0.0;


//For a single BH = momentum of the puncture
static double par_P[3] {
  0.4, // x offset
  0.4, // y offset
  0.0  // z offset
};

//"Constant value of radial shift near puncture."
static double beta_r_interior = 0;

// "Radial shift falloff radius."
static double beta_r_x0  = 0.25;

//"Radial shift falloff width."
static double beta_r_w =-1.;
