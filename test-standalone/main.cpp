#include "UIUCInitialData.h"

#include <iostream>

int main(int argc, char** argv) {
	UIUC_BlackHole id;
	
	std::cout << "Creating ID with: " << id.toString() << "\n";
	
	double pos[] = { 1., 1., 1. };
	double g[3][3], K[3][3], N[4];
	
	id.InitialData(pos, g, K, N);
	
	std::cout << "Output:\n" << id.outputToString(g,K,N);
}
