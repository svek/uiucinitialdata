# UIUC Rapidly Rotating Black Hole Initial Data

This repository holds the implementation of the UIUC or quasi-isotropic initial data.
For a paper reference, see Lieu, Etienne & Shapiro 2009, at [arXiv:1001.4077](https://arxiv.org/abs/1001.4077)
or [Phys.Rev.D80:121503](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.80.121503).

## Cactus Code Thorn UIUCInitialData

* Author(s): Ian Ruchlin
* Maintainer(s): Ian Ruchlin
* Licence      : FreeBSD, a.k.a., "2-Clause BSD"

Relvant files: All the `ccl` files and the code within the `src` repository.

## BHAC Standalone version

* Author(s): Elias Most (and Ian Ruchlin for the original version)

Relevant files: the `BHAC-version` directory.

To compile this, you need a bunch of other header files and BHAC macros.

## Real standalone pointwise version

* Author(s): Sven Köppel (and Ian Ruchlin for the original version)

For inclusion in an independent code with a pointwise state vector interface `void InitialData(double[3] x, double[ADMSized] Q)`.
This is helpful for instance in the ExaHyPE code or the Fortran prototype.

This version has no dependencies at all, it is a standalone C++ code.
