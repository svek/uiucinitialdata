// UIUCInitialData: File UIUCInitialData.h

//void UIUCInitialData();
void UIUC_ID(CCTK_REAL x, CCTK_REAL y, CCTK_REAL z, CCTK_REAL g[3][3], CCTK_REAL K[3][3], CCTK_REAL N[4]);
void QI_ID(CCTK_REAL x, CCTK_REAL y, CCTK_REAL z, CCTK_REAL g[3][3], CCTK_REAL K[3][3], CCTK_REAL N[4]);
void Lorentz_Transformation_3D(CCTK_REAL LT[3][3], CCTK_REAL iLT[3][3]);
